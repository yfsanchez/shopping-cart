import { Component, Input } from '@angular/core';
import { CartItem } from './cart-item';

@Component({
  selector: 'app-shopping-card-item',
  templateUrl: './shopping-card-item.component.html',
  styleUrls: ['./shopping-card-item.component.css']
})
export class ShoppingCardItemComponent {

  @Input()
  cartItem!: CartItem;

  constructor(){}

  ngOnInit(): void {}

}
