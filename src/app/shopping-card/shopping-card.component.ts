import { Component } from '@angular/core';
import { CartItem } from './cart-item';

@Component({
  selector: 'app-shopping-card',
  templateUrl: './shopping-card.component.html',
  styleUrls: ['./shopping-card.component.css']
})
export class ShoppingCardComponent {

  cartItems: CartItem[] = [
  {
    imageUrl: 'headphones.jpg',
    name: 'Auriculares',
    price: 99
  },
  {
    imageUrl: 'keyboard.jpg',
    name: 'Teclado',
    price: 79
  }
  ]

}
